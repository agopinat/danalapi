﻿



function addToCart(item) {
    //if cookie with JSON array of items doesn't exist, create JSON array coookie else take array. 
    //If item doesn't exist in array, add item to array else increment counter of item in array.

    //check if cookie with JSON array of items exits

    var htmlEncodedItem = item;
    var serializedItem = htmlDecode(htmlEncodedItem);
    var deserializedItem = jQuery.parseJSON(serializedItem);

    var itemsList = [];

    if ($.cookie('itemsList')) {
        var serItemsList = $.cookie('itemsList');
        var existingItemsInCartArray = JSON.parse(serItemsList);
        existingItemsInCartArray.push(deserializedItem);
        var serializedItemList = JSON.stringify(existingItemsInCartArray);
        $.cookie('itemsList', serializedItemList);

    }
    else {
        //serialize item object
        itemsList.push(deserializedItem)
        var serializedItemList = JSON.stringify(itemsList);

        //create itemsListCookie and add serializedItemList
        $.cookie('itemsList', serializedItemList);
    }

    //update costs

    UpdateCosts();
    window.location.href = "/Account/Cart";
    
}

function htmlDecode(value) {
    return $('<div/>').html(value).text();
}


function UpdateCosts() {
    if ($('.price').length > 0) {
        if ($('.price').length == 1) {
            var price = parseInt($('.price')[0].innerText);
            var units = parseInt($('.units')[0].value);
            var subtotal = price * units;
            var taxes = (0.0725 * subtotal).toFixed(2);
            var total = parseInt(subtotal) + parseFloat(taxes) + parseInt(20);

        }
        else if ($('.price').length > 1) {
            var array = $('.price');
            var price = 0;
            var units = 0;
            var subtotal = 0;
            var taxes = 0;
            var total = 0;
            $.each(array, function (index, value) {
                price = parseInt($('.price')[index].innerText);
                units = parseInt($('.units')[index].value);
                subtotal = subtotal + (price * units);
                taxes = (0.0725 * subtotal).toFixed(2);
                total = parseInt(subtotal) + parseFloat(taxes) + parseInt(20);
            })
        }

        $('.subtotal').text(subtotal);
        $('.shippingcost').text('$20');
        $('.taxes').text(taxes);
        $('.total').text(total);
    }
    else {
        $('.subtotal').text('0');
        $('.shippingcost').text('0');
        $('.taxes').text('0');
        $('.total').text('0');
    }
}




